import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import { FormValidationMessage, FormInput, Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import { validate } from "../../utils/validation";
import Toast from 'react-native-easy-toast'
import Constants from "../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
const Logo = require("../../assets/images/ic_logo.png");

class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      emailError: null,
      waiting: false
    };
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  sendEmail = () => {
    const self = this;

    let { email } = this.state;
    let emailError = validate("email", email);
    this.setState({
      emailError: emailError
    });

    if (emailError) {      
      this.refs.toast.show("Check your email address.");
      return;
    }

    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      email: self.state.email
    };

    this.setState({
      waiting: true
    });
    axios
      .post(
        Constants.webserviceName + "/api/auth/forgot_password",
        payload,
        config
      )
      .then(async response => {        
        let message = response.data.data.message;        
        self.setState({
          waiting: false
        });                
        self.props.navigation.navigate("VerificationCode", {email: self.state.email});
      })
      .catch(error => {        
        self.refs.toast.show("Error happend. Please check your email address and try again."); 
        self.setState({
          waiting: false
        });
      });
  };

  render() {
    const { emailError, waiting } = this.state;
    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Forgot Password</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>        
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            <View style={styles.logo_wrapper}>
              <Image style={styles.logo_image} source={Logo} />
              <Text style={styles.logoTitle}>Psychic Contact</Text>
            </View>
            <View style={styles.contentContainer}>
              <Text style={styles.forgotPasswordDescription}>
                Enter your email address below and we will email you a link to
                reset your password
              </Text>
              <View style={styles.inputContainer}>
                <MaterialCommunityIcon
                  name="email-outline"
                  size={16}
                  color={colors.standardColor}
                />
                <TextInput
                  style={styles.inputs}
                  placeholderTextColor={colors.standardColor}
                  keyboardType="email-address"
                  placeholder="Email"
                  onChangeText={value =>
                    this.setState({ email: value }, function() {
                      console.log(this.state.email);
                    })
                  }
                  value={this.state.email}
                />
              </View>
              <Text style={styles.underline} />
              {emailError ? (
                <FormValidationMessage
                  labelStyle={styles.formValidationMessage}
                >
                  {emailError}
                </FormValidationMessage>
              ) : null}

              <TouchableOpacity
                style={[styles.controlButtonContainer, { marginTop: 40 }]}
                onPress={this.sendEmail}
              >
                <LinearGradient
                  style={styles.controlButtonLinearGradient}
                  start={{ x: -0.2, y: 1.5 }}
                  end={{ x: 1.2, y: -0.5 }}
                  colors={[colors.linearStarColor, colors.linearEndColor]}
                >
                  <Text style={styles.controlButtonText}>
                    NEXT
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>        
        <Toast ref="toast"/>  
      </MainScreen>
    );
  }
}

export default ForgotPasswordScreen;
