import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import { FormValidationMessage, FormInput, Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import { validate } from "../../utils/validation";
import Toast from 'react-native-easy-toast'
import Constants from "../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
const Logo = require("../../assets/images/ic_logo.png");

class ResetPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      retypePassword: "",
      newPasswordError: null,
      retypePasswordError: null,
      waiting: false
    };
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  resetPassword = () => {
    // this.props.navigation.popToTop();
    const self = this;

    let {
      newPassword,
      retypePassword,      
    } = this.state;
    
    let newPasswordError = validate("password", newPassword);
    let retypePasswordError = validate("password", retypePassword);    

    this.setState({            
      newPasswordError: newPasswordError,
      retypePasswordError: retypePasswordError,      
    });

    if ( newPasswordError || retypePasswordError ) {      
      this.refs.toast.show("Check your input fields.");
      return;
    }

    if (newPassword !== retypePassword) {
      this.setState({
        retypePasswordError: "Password mismatched"
      });      
      return;
    }

    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      email: this.props.navigation.state.params.email,      
      password: newPassword,
      password_confirmation: retypePassword,      
    };

    this.setState({
      waiting: true
    });
    axios
      .post(Constants.webserviceName + "/api/auth/password_reset", payload, config)
      .then(response => {
        let status = response.data.data.status;
        let message = response.data.data.message;

        self.setState({
          waiting: false
        });
        if (status === "success") {          
          self.props.navigation.popToTop();
        }        
      })
      .catch(error => {
        let message = JSON.parse(error.request.response)        
        
        let newPasswordError = message.errors.detail.password ? message.errors.detail.password : null;
        let retypePasswordError = message.errors.detail.password_confirmation ? message.errors.detail.password_confirmation : null;        
        
        self.setState({
          waiting: false,
          newPasswordError: newPasswordError,          
          retypePasswordError: retypePasswordError,          
        });                
        self.refs.toast.show("Error happend. Please check your inputs and try again.");
      });

  };

  render() {
    const {
      newPassword,
      newPasswordError,
      retypePassword,
      retypePasswordError,
      waiting
    } = this.state;
    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Reset Password</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            <View style={styles.logo_wrapper}>
              <Image style={styles.logo_image} source={Logo} />
              <Text style={styles.logoTitle}>Psychic Contact</Text>
            </View>
            <View style={styles.contentContainer}>
              <View style={styles.inputContainer}>
                <MaterialCommunityIcon
                  name="lock-outline"
                  size={16}
                  color={colors.standardColor}
                />
                <TextInput
                  style={styles.inputs}
                  placeholderTextColor={colors.standardColor}
                  placeholder="New Password"
                  secureTextEntry
                  onChangeText={value => this.setState({ newPassword: value })}
                  value={this.state.newPassword}
                />
              </View>
              <Text style={styles.underline} />
              {newPasswordError ? (
                <FormValidationMessage
                  labelStyle={styles.formValidationMessage}
                >
                  {newPasswordError}
                </FormValidationMessage>
              ) : null}

              <View style={styles.inputContainer}>
                <MaterialCommunityIcon
                  name="lock-outline"
                  size={16}
                  color={colors.standardColor}
                />
                <TextInput
                  style={styles.inputs}
                  placeholderTextColor={colors.standardColor}
                  placeholder="Re-Type Password"
                  secureTextEntry
                  onChangeText={value =>
                    this.setState({ retypePassword: value })
                  }
                  value={this.state.retypePassword}
                />
              </View>
              <Text style={styles.underline} />
              {retypePasswordError ? (
                <FormValidationMessage
                  labelStyle={styles.formValidationMessage}
                >
                  {retypePasswordError}
                </FormValidationMessage>
              ) : null}

              <TouchableOpacity
                style={[styles.controlButtonContainer, { marginTop: 40 }]}
                onPress={this.resetPassword}
              >
                <LinearGradient
                  style={styles.controlButtonLinearGradient}
                  start={{ x: -0.2, y: 1.5 }}
                  end={{ x: 1.2, y: -0.5 }}
                  colors={[colors.linearStarColor, colors.linearEndColor]}
                >
                  <Text style={styles.controlButtonText}>RESET PASSWORD</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Toast ref="toast"/>        
      </MainScreen>
    );
  }
}

export default ResetPasswordScreen;
