import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import { FormValidationMessage, FormInput, Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import { validate } from "../../utils/validation";
import Toast from 'react-native-easy-toast'
import Constants from "../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
const Logo = require("../../assets/images/ic_logo.png");

class VerificationCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      verificationCode: "",
      verificationCodeError: null,
      waiting: false
    };
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  verifyCode = () => {    
    const self = this;

    let { verificationCode } = this.state;
    let verificationCodeError = validate("number", verificationCode);
    this.setState({
      verificationCodeError: verificationCodeError
    });

    if (verificationCodeError) {      
      this.refs.toast.show("Verification Code should be Number.");
      return;
    }

    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      email: this.props.navigation.state.params.email,
      token: self.state.verificationCode
    };

    this.setState({
      waiting: true
    });
    axios
      .post(
        Constants.webserviceName + "/api/auth/forgot_password_check",
        payload,
        config
      )
      .then(async response => {
        let status = response.data.data.status;
        let message = response.data.data.message;
        self.setState({
          waiting: false
        });        
        if (status === "success") {
          self.props.navigation.navigate("ResetPassword", {email: this.props.navigation.state.params.email});
        }        
        self.refs.toast.show(message);
      })
      .catch(error => {        
        self.refs.toast.show("Error happend. Please check your Verification Code and try again.");
        console.log(error);
        self.setState({
          waiting: false
        });
      });
  };

  render() {
    const { verificationCodeError, waiting } = this.state;
    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Verification Code</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            <View style={styles.logo_wrapper}>
              <Image style={styles.logo_image} source={Logo} />
              <Text style={styles.logoTitle}>Psychic Contact</Text>
            </View>
            <View style={styles.contentContainer}>
              <Text style={styles.forgotPasswordDescription}>
                Enter the digit code that we have sent in your Email Address.
              </Text>
              <View style={styles.inputContainer}>
                <MaterialCommunityIcon
                  name="lock-outline"
                  size={16}
                  color={colors.standardColor}
                />
                <TextInput
                  style={styles.inputs}
                  placeholderTextColor={colors.standardColor}
                  keyboardType="numeric"
                  secureTextEntry
                  placeholder="Verification Code"
                  onChangeText={value =>
                    this.setState({ verificationCode: value }, function() {
                      console.log(this.state.verificationCode);
                    })
                  }
                  value={this.state.verificationCode}
                />
              </View>
              <Text style={styles.underline} />
              {verificationCodeError ? (
                <FormValidationMessage
                  labelStyle={styles.formValidationMessage}
                >
                  {verificationCodeError}
                </FormValidationMessage>
              ) : null}

              <TouchableOpacity
                style={[styles.controlButtonContainer, { marginTop: 40 }]}
                onPress={this.verifyCode}
              >
                <LinearGradient
                  style={styles.controlButtonLinearGradient}
                  start={{ x: -0.2, y: 1.5 }}
                  end={{ x: 1.2, y: -0.5 }}
                  colors={[colors.linearStarColor, colors.linearEndColor]}
                >
                  <Text style={styles.controlButtonText}>
                    NEXT
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>    
        <Toast ref="toast"/>     
      </MainScreen>
    );
  }
}

export default VerificationCodeScreen;
