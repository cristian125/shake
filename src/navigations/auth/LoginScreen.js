import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator,
} from "react-native";
import { FormValidationMessage, FormInput, Input } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import { validate } from "../../utils/validation";
import Constants from "../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import Toast from 'react-native-easy-toast'

const Logo = require("../../assets/images/ic_logo.png");

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailError: null,
      passwordError: null,
      waiting: false
    };

    this.loginUser = this.loginUser.bind(this);
  }
  static navigationOptions = {
    title: "Login",
    header: null
  };
  loginUser = () => {    
    const self = this;

    let { email, password } = this.state;
    let emailError = validate("input", email);
    let passwordError = validate("password", password);
    this.setState({
      emailError: emailError,
      passwordError: passwordError
    });

    if (emailError || passwordError) {      
      this.refs.toast.show("Check your input fields.");
      return;
    }

    // this.props.navigation.navigate('App');

    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      username: self.state.email,
      password: self.state.password
    };

    this.setState({
      waiting: true
    });
    axios
      .post(Constants.webserviceName + "/api/auth/login", payload, config)
      .then(async response => {        
        await AsyncStorage.setItem("userToken", response.data.access_token);
        self.setState({
          waiting: false
        });                
        self.props.navigation.navigate("App");
      })
      .catch(error => {                
        self.setState({
          waiting: false
        });
        let message = JSON.parse(error.request.response).message;
        self.refs.toast.show(message ? message  : "Error happend. Please check Username and Password again.");        
      });
  };

  render() {
    const { emailError, passwordError, waiting } = this.state;
    return (
      <MainScreen waiting={waiting}>      
       <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
        <View style={styles.scroll_content_wrapper}>
          <View style={styles.logo_wrapper}>
            <Image style={styles.logo_image} source={Logo} />
            <Text style={styles.logoTitle}>Psychic Contact</Text>
          </View>
          <View style={styles.contentContainer}>
            <View style={styles.inputContainer}>
            <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}                
                placeholder="Username"
                onChangeText={value =>
                  this.setState({ email: value }, function() {
                    console.log(this.state.email);
                  })
                }
                value={this.state.email}
              />
            </View>
            <Text style={styles.underline} />
            {emailError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {emailError}
              </FormValidationMessage>
            ) : null}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Password"
                secureTextEntry
                onChangeText={value => this.setState({ password: value })}
                value={this.state.password}
              />
            </View>
            <Text style={styles.underline} />
            {passwordError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {passwordError}
              </FormValidationMessage>
            ) : null}

            <View style={styles.signupForgotPasswordContainer}>
            <Text style={styles.signupDescription}>New user? </Text>
              <TouchableOpacity
                style={styles.signupButtonContainer}
                onPress={() => this.props.navigation.navigate("SignUp")}
              >
                <Text style={styles.signUpButtonText}>Signup</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.forgotButtonContainer}
                onPress={() => this.props.navigation.navigate("ForgotPassword")}
              >
                <Text style={styles.forgotButtonText}>Forgot password?</Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={styles.controlButtonContainer}
              onPress={this.loginUser}
            >
              <LinearGradient
                style={styles.controlButtonLinearGradient}
                start={{ x: -0.2, y: 1.5 }}
                end={{ x: 1.2, y: -0.5 }}
                colors={[colors.linearStarColor, colors.linearEndColor]}
              >
                <Text style={styles.controlButtonText}>LOGIN</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>          
       </KeyboardAwareScrollView>     
       <Toast ref="toast"/>   
      </MainScreen>
    );
  }
}

export default LoginScreen;
