import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {
  FormValidationMessage,
  FormInput,
  Input,
  CheckBox,
  Icon
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors, fonts } from "../../styles/styles";
import { MainScreen } from "../../components";
import { validate } from "../../utils/validation";
import Toast from 'react-native-easy-toast'
import Constants from "../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import SelectionModal from "../../components/SelectionModal";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FeatherIcon from "react-native-vector-icons/Feather";
import DatePicker from 'react-native-date-picker';
import moment from 'moment';

const Logo = require("../../assets/images/ic_logo_title.png");

class SignupScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      username: "",
      password: "",
      passwordConfirmation: "",
      firstName: "",
      lastName: "",
      gender: "",
      birthDate: null,
      country: "",
      newsletter: null,
      terms: null,

      emailError: null,
      usernameError: null,
      passwordError: null,
      passwordConfirmationError: null,
      firstNameError: null,
      lastNameError: null,
      genderError: null,
      birthDateError: null,
      countryError: null,

      waiting: false,
      isDisplayModal: false,
      isCountrySelected: false,
      countryList: [],
      isCalenderShown: false
    };

    this.registerUser = this.registerUser.bind(this);
    this.onItemSelected = this.onItemSelected.bind(this);
    this.getBirthDateString = this.getBirthDateString.bind(this);
  }
  
  static navigationOptions = {
    title: "",
    header: null
  };

  componentDidMount() {
    this.getCountryList();
  }
  
  getCountryList = () => {
    const self = this;    
    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    var countryList = [];
    axios
      .get(Constants.webserviceName + "/api/auth/country", config)
      .then( response => {                
        for (var index = 0; index < response.data.data.length; index++) {
          countryList.push(response.data.data[index].name)
        }     
        self.setState({ countryList: countryList})   
      })
      .catch(error => {
        console.warn(error)
      });
    }

  registerUser = () => {
    const self = this;

    let {
      email,
      username,
      password,
      passwordConfirmation,
      firstName,
      lastName,
      gender,
      birthDate,
      country,
      newsletter,
      terms
    } = this.state;

    let emailError = validate("email", email);
    let usernameError = validate("input", username);
    let passwordError = validate("password", password);
    let passwordConfirmationError = validate("password", passwordConfirmation);
    let firstNameError = validate("input", firstName);
    let lastNameError = validate("input", lastName);
    let genderError = validate("input", gender);
    let birthDateError = validate("input", this.getBirthDateString());
    let countryError = validate("input", country);

    this.setState({
      emailError: emailError,
      usernameError: usernameError,
      passwordError: passwordError,
      passwordConfirmationError: passwordConfirmationError,
      firstNameError: firstNameError,
      lastNameError: lastNameError,
      genderError: genderError,
      birthDateError: birthDateError,
      countryError: countryError,      
      newsletter: newsletter ? newsletter : false,
      terms: terms ? terms : false
    });

    if (
      emailError ||
      passwordError ||
      usernameError ||
      passwordConfirmationError ||
      firstNameError ||
      lastNameError ||
      genderError ||
      birthDateError ||
      countryError ||
      !newsletter ||
      !terms
    ) {      
      this.refs.toast.show("Check your input fields.");
      return;
    }

    if (passwordConfirmation !== password) {
      this.setState({
        passwordConfirmationError: "Password mismatched"
      });      
      this.refs.toast.show("Check your input fields.");
      return;
    }

    // this.props.navigation.navigate('App');

    let config = {
      headers: {
        Authorization: "Bearer " + Constants.Bearer_Token,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      email: email,
      username: username,
      password: password,
      password_confirmation: passwordConfirmation,
      first_name: firstName,
      last_name: lastName,
      gender: gender,
      country: country,
      dob: this.getBirthDateString(),      
    };

    this.setState({
      waiting: true
    });
    axios
      .post(Constants.webserviceName + "/api/auth/register", payload, config)
      .then(response => {
        self.setState({
          waiting: false
        });   
        self.props.navigation.goBack();
      })
      .catch(error => {
        let message = JSON.parse(error.request.response)        
        
        let emailError = message.errors.detail.email ? message.errors.detail.email : null;
        let usernameError = message.errors.detail.username ? message.errors.detail.username : null;
        let passwordError = message.errors.detail.password ? message.errors.detail.password : null;
        let passwordConfirmationError = message.errors.detail.password_confirmation ? message.errors.detail.password_confirmation : null;
        let firstNameError = message.errors.detail.first_name ? message.errors.detail.first_name : null;
        let lastNameError = message.errors.detail.last_name ? message.errors.detail.last_name : null;
        let genderError = message.errors.detail.gender ? message.errors.detail.gender : null;
        let birthDateError = message.errors.detail.dob ? message.errors.detail.dob : null;
        let countryError = message.errors.detail.country ? message.errors.detail.country : null;
        
        self.setState({
          waiting: false,
          emailError: emailError,
          usernameError: usernameError,
          passwordError: passwordError,
          passwordConfirmation: passwordConfirmation,
          passwordConfirmationError: passwordConfirmationError,
          firstNameError: firstNameError,
          lastNameError: lastNameError,
          genderError: genderError,
          birthDateError: birthDateError,
          countryError: countryError
        });        
        self.refs.toast.show("Error happend. Please check your inputs and try again.");
      });
  };

  onCloseSelectionModal = () => {
    this.setState({ isDisplayModal: false });
  };

  onItemSelected(value) {
    if (this.state.isCountrySelected)
      this.setState({ country: value });
    else
      this.setState({ gender: value });
  }

  getBirthDateString() {
    if (!this.state.birthDate)
      return "";

    var dateString = moment(this.state.birthDate).format('YYYY-MM-DD');    
    return dateString;
  }
  render() {
    const {
      waiting,
      emailError,
      usernameError,
      passwordError,
      passwordConfirmationError,
      firstNameError,
      lastNameError,
      genderError,
      birthDateError,
      countryError,
      newsletter,
      terms,
      isDisplayModal,
      isCountrySelected,
      countryList,
      isCalenderShown
    } = this.state;

    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Registration</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>          
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            {/* Email Address */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="email-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                keyboardType="email-address"
                placeholder="Email"
                onChangeText={value =>
                  this.setState({ email: value }, function() {
                    console.log(this.state.email);
                  })
                }
                value={this.state.email}
              />
            </View>
            <Text style={styles.underline} />
            {emailError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {emailError}
              </FormValidationMessage>
            ) : null}

            {/* Username */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Username"
                onChangeText={value =>
                  this.setState({ username: value }, function() {
                    console.log(this.state.username);
                  })
                }
                value={this.state.username}
              />
            </View>
            <Text style={styles.underline} />
            {usernameError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {usernameError}
              </FormValidationMessage>
            ) : null}

            {/* Password */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Password"
                secureTextEntry
                onChangeText={value => this.setState({ password: value })}
                value={this.state.password}
              />
            </View>
            <Text style={styles.underline} />
            {passwordError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {passwordError}
              </FormValidationMessage>
            ) : null}

            {/* Password Confirmation */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Re-Type Password"
                secureTextEntry
                onChangeText={value =>
                  this.setState({ passwordConfirmation: value })
                }
                value={this.state.passwordConfirmation}
              />
            </View>
            <Text style={styles.underline} />
            {passwordConfirmationError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {passwordConfirmationError}
              </FormValidationMessage>
            ) : null}

            {/* First Name */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="First Name"
                onChangeText={value =>
                  this.setState({ firstName: value }, function() {})
                }
                value={this.state.firstName}
              />
            </View>
            <Text style={styles.underline} />
            {firstNameError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {firstNameError}
              </FormValidationMessage>
            ) : null}

            {/* Last Name */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Last Name"
                onChangeText={value =>
                  this.setState({ lastName: value }, function() {})
                }
                value={this.state.lastName}
              />
            </View>
            <Text style={styles.underline} />
            {lastNameError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {lastNameError}
              </FormValidationMessage>
            ) : null}

            {/* Gender */}
            <View style={styles.inputContainer}>
              <FontAwesomeIcon
                name="venus-mars"
                size={14}
                color={colors.standardColor}
              />
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() => this.setState({ isDisplayModal: true, isCountrySelected: false })}
              >
                <Text
                  style={{
                    color: this.state.gender ? "black" : colors.standardColor
                  }}
                >
                  {this.state.gender ? this.state.gender : "Gender"}
                </Text>
                <FeatherIcon
                  name="chevron-down"
                  size={16}
                  color={colors.redColor}
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {genderError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {genderError}
              </FormValidationMessage>
            ) : null}

            {/* BirthDate */}
            <View style={styles.inputContainer}>
              <FeatherIcon
                name="calendar"
                size={14}
                color={colors.standardColor}
              />              
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() => this.setState({ isCalenderShown: !this.state.isCalenderShown })}
              >
                <Text
                  style={{
                    color: this.getBirthDateString() ? "black" : colors.standardColor
                  }}
                >
                  {this.getBirthDateString() ? this.getBirthDateString() : "Date of Birth"}
                </Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {birthDateError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {birthDateError}
              </FormValidationMessage>
            ) : null}

            {isCalenderShown && <DatePicker
                date={this.state.birthDate ? this.state.birthDate : new Date(moment(new Date()).format("YYYY-MM-DD"))}
                style={styles.datepicker}
                mode={'date'}
                onDateChange={date => this.setState({ birthDate: date })}
            /> }

            {/* Country */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="home-outline"
                size={16}
                color={colors.standardColor}
              />
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() => this.setState({ isDisplayModal: true, isCountrySelected: true })}
              >
                <Text
                  style={{
                    color: this.state.country ? "black" : colors.standardColor
                  }}
                >
                  {this.state.country ? this.state.country : "Country"}
                </Text>
                <FeatherIcon
                  name="chevron-down"
                  size={16}
                  color={colors.redColor}
                />
                </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {countryError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {countryError}
              </FormValidationMessage>
            ) : null}

            {/* Newsletter */}
            <View style={styles.inputContainer}>
              <FontAwesomeIcon
                name="newspaper-o"
                size={14}
                color={colors.standardColor}
              />
              <CheckBox
                iconRight
                title="Newsletter"
                textStyle={styles.checkbox_text}
                containerStyle={styles.checkbox_container}
                checked={this.state.newsletter}
                onPress={() => this.setState({ newsletter: !newsletter })}
                checkedColor={colors.redColor}
                uncheckedColor={colors.standardColor}
                size={16}
              />
            </View>
            {newsletter === false ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {"This Field is required to be checked."}
              </FormValidationMessage>
            ) : null}

            {/* Terms */}
            <View style={[styles.inputContainer, { marginTop: 0 }]}>
              <FontAwesomeIcon
                name="newspaper-o"
                size={14}
                color={colors.standardColor}
              />
              <CheckBox
                iconRight
                title="Terms"
                textStyle={styles.checkbox_text}
                containerStyle={styles.checkbox_container}
                checked={this.state.terms}
                onPress={() => this.setState({ terms: !terms })}
                checkedColor={colors.redColor}
                uncheckedColor={colors.standardColor}
                size={16}
              />
            </View>
            {terms === false ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {"This Field is required to be checked."}
              </FormValidationMessage>
            ) : null}

            <Text style={styles.terms_conditions_title}>
              I have read and agreed to all the Member
            </Text>
            <TouchableOpacity>
              <Text style={styles.btn_terms_conditions}>
                Terms and Conditions
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[styles.controlButtonContainer, { marginTop: 30 }]}
              onPress={this.registerUser}
            >
              <LinearGradient
                style={styles.controlButtonLinearGradient}
                start={{ x: -0.2, y: 1.5 }}
                end={{ x: 1.2, y: -0.5 }}
                colors={[colors.linearStarColor, colors.linearEndColor]}
              >
                <Text style={styles.controlButtonText}>REGISTER</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>        

        <SelectionModal
          isShown={isDisplayModal}
          closeModal
          closeModal={this.onCloseSelectionModal}
          list={isCountrySelected ? countryList : ["Female", "Male", "Other"]}
          value={isCountrySelected ? this.state.country : this.state.gender}
          itemClicked={value => this.onItemSelected(value)}
        /> 
        <Toast ref="toast"/> 
      </MainScreen>
    );
  }
}

export default SignupScreen;
