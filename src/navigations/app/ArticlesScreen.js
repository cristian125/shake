import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  Button
} from "react-native";
import styles from "../../styles/styles";
import { MainScreen } from "../../components";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

class ArticlesScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: "Articles",
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <FontAwesomeIcon name="newspaper-o" size={20} color={tintColor} />
    )
  };

  render() {
    return (
      <MainScreen>
        <View style={styles.logo_wrapper}>
          <Text style={styles.logoTitle}>Articles!!!</Text>
        </View>
      </MainScreen>
    );
  }
}

export default ArticlesScreen;
