import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  Button
} from "react-native";
import styles from "../../styles/styles";
import { MainScreen } from "../../components";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";

class OurReadersScreen extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: "Our Readers",
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <MaterialCommunityIcon name="cards-outline" size={24} color={tintColor} />
    )
  };

  render() {
    return (
      <MainScreen>
        <View style={styles.logo_wrapper}>
          <Text style={styles.logoTitle}>Our Readers!!!</Text>
        </View>
      </MainScreen>
    );
  }
}

export default OurReadersScreen;
