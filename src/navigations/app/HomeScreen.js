import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  Button,
  ScrollView
} from "react-native";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import FeatherIcon from "react-native-vector-icons/Feather";
import LinearGradient from "react-native-linear-gradient";
import RenderList from "../../components/RenderList";
import CategoryList from "../../components/CategoryList";
import PostList from "../../components/PostList";
import FeaturedReaderList from "../../components/FeaturedReaderList";
import PromotionList from "../../components/PromotionList";

const menuIcon = require("../../assets/images/ico_menu.png");

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ""
    };
  }

  static navigationOptions = {
    title: "Home",
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <FeatherIcon name="home" size={22} color={tintColor} />
    )
  };

  render() {
    const { searchText } = this.state;

    return (
      <MainScreen>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarLeftTitle}>Home</Text>

            <TouchableOpacity
              onPress={() => this.props.navigation.toggleDrawer()}
              style={styles.menuButton}
            >
              <Image source={menuIcon} style={styles.menuIcon} />
            </TouchableOpacity>
          </LinearGradient>
          <View style={styles.home_search_container}>
            <FeatherIcon name="search" size={16} color={colors.standardColor} style={styles.home_search_icon}/>
            <TextInput
              style={[styles.inputs, {paddingLeft: 0}]}
              placeholderTextColor={colors.standardColor}
              placeholder="Search"
              onChangeText={value => this.setState({ searchText: value })}
              value={searchText}
            />
          </View>
        </View>
        
        <ScrollView showsVerticalScrollIndicator={false} style={{marginTop: 100, backgroundColor:"#f5f6f8"}}>
            <View style={{height: 30}} />
            <RenderList title={"Our Readers"}/>
            <CategoryList title={"Category"}/>
            <PostList title={"Latest Post from Readers"} />      
            <FeaturedReaderList title={"Featured Reader"} />
            <PostList title={"Most Reviewed Post"} />    
            <PromotionList title={"Promotion"} />        
        </ScrollView>
        
      </MainScreen>
    );
  }
}

export default HomeScreen;
