import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  Button,
  ScrollView,
  AsyncStorage
} from "react-native";
import styles, { colors } from "../../styles/styles";
import { MainScreen } from "../../components";
import FeatherIcon from "react-native-vector-icons/Feather";
import LinearGradient from "react-native-linear-gradient";
import AccountItem from "../../components/AccountItem";
import Constants from "../../utils/constants";

const menuIcon = require("../../assets/images/ico_menu.png");

class AccountScreen extends Component {
  isCancelled = false;

  constructor(props) {
    super(props);
    this.state = {
      userInfo: null
    };

    this.onLogOut = this.onLogOut.bind(this);
    this.getUser = this.getUser.bind(this);
  }

  static navigationOptions = {
    title: "Account",
    header: null,
    tabBarIcon: ({ tintColor }) => (
      <FeatherIcon name="user" size={22} color={tintColor} />
    )
  };

  componentWillMount() {    
    this.isCancelled = false;
    this.getUser();
  }

  componentWillUnmount() {
    this.isCancelled = true;
  }

  getUser = async () => {
    const self = this;
    var userToken = "";
    await AsyncStorage.getItem("userToken").then(value => {
      if(value) {
        userToken = value;
      }
    });
        
    let config = {
      headers: {
        Authorization: "Bearer " + userToken,
        "Content-Type": "application/json"
      }
    };

    if(! this.isCancelled) {
      this.setState({
        waiting: true
      });
    }
    axios
      .get(Constants.webserviceName + "/api/auth/user", config)
      .then(async response => {
        if(! this.isCancelled) {
          self.setState({
            waiting: false,
            userInfo: response.data
          });
        }
      })
      .catch(error => {
        if(! this.isCancelled) {
          self.setState({
            waiting: false
          });
        }
      });
  };

  async onLogOut() {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  }

  render() {
    const { userInfo } = this.state;

    return (
      <MainScreen>
        <View style={[styles.topbar, { height: 250 }]}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: 0, y: 1 }}
            end={{ x: 1.4, y: 0.4 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarLeftTitle} />
            <View style={styles.profile_container}>
              <View style={styles.profile_avatar_container}>
                <Image source={userInfo && userInfo.profile_image ? {uri: Constants.webserviceName + "/image/" + userInfo.profile_image} : null} style={styles.profile_avatar} />
                <View style={styles.profile_online_status} />
              </View>
              <Text style={styles.user_fullname}>{userInfo && userInfo.first_name && userInfo.last_name ? userInfo.first_name + " " + userInfo.last_name : null}</Text>
              <Text style={styles.username}>{userInfo && userInfo.username ? "username: " + userInfo.username : null}</Text>
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.toggleDrawer()}
              style={[
                styles.menuButton,
                { height: 100, alignSelf: "flex-start" }
              ]}
            >
              <Image source={menuIcon} style={styles.menuIcon} />
            </TouchableOpacity>
          </LinearGradient>
          <View style={styles.profile_status_view}>
            <View style={styles.profile_status_item}>
              <Text style={styles.profile_status_value}>02:00</Text>
              <Text style={styles.profile_status_title}>Chat Time</Text>
            </View>
            <View style={styles.profile_status_item}>
              <Text style={styles.profile_status_value}>00:00</Text>
              <Text style={styles.profile_status_title}>Free Mins</Text>
            </View>
            <View style={styles.profile_status_item}>
              <Text style={styles.profile_status_value}>$0.00</Text>
              <Text style={styles.profile_status_title}>Email Funds</Text>
            </View>
          </View>
        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{ marginTop: 290, paddingLeft: 20, paddingRight: 20 }}
        >
          <AccountItem
            iconType={"fund"}
            title={"Fund My Account"}
            description={"Select a package and fund your account"}
          />
          <AccountItem
            iconType={"nrr"}
            title={"NRR Request Form"}
            description={"Requesting time back from the reader"}
          />
          <AccountItem
            iconType={"email"}
            title={"My Email Readings"}
            description={"Change your profile details"}
          />
          <AccountItem
            iconType={"billing"}
            title={"Billing & Transaction"}
            description={"Previous billing and transactions"}
          />
          <AccountItem
            iconType={"history"}
            title={"Chat History"}
            description={"Check your previous chat"}
          />
          <AccountItem
            iconType={"message"}
            title={"Message Center"}
            description={"Check all your messages"}
          />
          <AccountItem
            iconType={"account"}
            title={"My Account"}
            description={"Change your profile details"}
            onPressItem={() => this.props.navigation.navigate("Profile", {getUser: this.getUser})}
          />
          <AccountItem
            iconType={"security"}
            title={"Security Settings"}
            description={"Change your password and security question"}
            onPressItem={() => this.props.navigation.navigate("SecuritySettings")}
          />
          <AccountItem
            iconType={"notification"}
            title={"Notification Board"}
            description={"Notification Settings"}
          />
          <AccountItem
            iconType={"logout"}
            title={"Logout"}
            description={"Sign out from your account"}
            onPressItem={() => this.onLogOut()}
          />
        </ScrollView>
      </MainScreen>
    );
  }
}

export default AccountScreen;
