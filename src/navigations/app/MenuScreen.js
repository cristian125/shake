import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  AsyncStorage,
  ActivityIndicator,
  ScrollView
} from "react-native";
import styles, { colors } from "../../styles/styles";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import { DrawerActions } from 'react-navigation';
import LinearGradient from "react-native-linear-gradient";

class MenuScreen extends Component {
  constructor(props) {
    super(props);
  }

  navigateToScreen = (route) => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }
  
  render() {
    return (
      <View style={styles.menu_container}>
      <LinearGradient
            style={styles.menu_linear_bg}
            start={{ x: 0.5, y: 1 }}
            end={{ x: 0.5, y: 0 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
        <TouchableOpacity onPress={() => this.props.navigation.dispatch(DrawerActions.closeDrawer())} style={styles.menu_close}>
          <MaterialCommunityIcon name="close" size={24} color="white" />
        </TouchableOpacity>
        <ScrollView showsVerticalScrollIndicator={false} />
        </LinearGradient>
      </View>
    );
  }
}

export default MenuScreen;
