import React, { Component } from 'react';
import { ScrollView, TextInput, Text, KeyboardAvoidingView, TouchableOpacity, View, StatusBar, Image, Picker, Platform, AsyncStorage } from 'react-native';
import axios from 'axios';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import styles from '../../styles/styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker'
import DatePicker from 'react-native-datepicker'
import IOSPicker from 'react-native-ios-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { MainScreen } from '../../components';
import { validate } from '../../utils/validation'
import Toast from 'react-native-simple-toast';
import Constants from '../../utils/constants'

var fields = [
    { "key":"email", "title": "Email", "required": true, "type": "input", "validationType": "email" },
    { "key":"handle", "title": "Handle", "required": true, "type": "input", "validationType": "input" },
    { "key":"firstname", "title": "First Name", "required": true, "type": "input", "validationType": "input" },
    { "key":"lastname", "title": "Last Name", "required": true, "type": "input", "validationType": "input" },
    { "key":"birthdate", "title": "Birthdate", "required": true, "type": "datepicker", "validationType": "input" },
    { "key":"gender", "title": "Gender", "required": true, "type": "picker", "validationType": "input", "value": [{"label" : "Please Select", "value" : ""} , {"label" : "Female", "value" : "Female"}, {"label" : "Male", "value" : "Male"}, {"label" : "Other", "value" : "Other"}] },
    { "key":"mobile", "title": "Mobile", "required": true, "type": "input", "validationType": "number" },
    { "key":"street", "title": "Address", "required": true, "type": "input", "validationType": "input" },    
    { "key":"state_id", "title": "State", "required": true, "type": "picker", "validationType": "input", "value": [{"label" : "Please Select", "value" : ""}] },
    { "key":"city", "title": "City", "required": true, "type": "input", "validationType": "input" },    
    { "key":"country_id", "title": "Country", "required": true, "type": "picker", "validationType": "input", "value": [{"label" : "Please Select", "value" : ""}] },
    { "key":"zip", "title": "Zip", "required": true, "type": "input", "validationType": "number" },
    { "key":"password", "title": "Password", "required": true, "type": "input", "security": true, "validationType": "input" },
    { "key":"password_confirmation", "title": "Confirm Password", "required": true, "type": "input", "security": true, "validationType": "input" },
    ];

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            waiting: false,
            pickedImage: "",
            registerData: {

            },
            registerDataError: {

            },
        }

        this.getUser = this.getUser.bind(this)
    }

    static navigationOptions = {
        title: 'Profile',
    };

    componentWillMount(){
        this.getUser()
    }

    getUser = async() => {
        const { registerDataError, registerData } = this.state
        const self = this
        var userToken = ""
        await AsyncStorage.getItem('userToken')
        .then((value) => {            
            userToken = value
        });
        
        let config = {
            headers: {
                'Authorization': 'Bearer ' + userToken,
                'Content-Type': 'application/json'
            }
        }

        this.setState({
            waiting: true
        })
        axios.get(Constants.webserviceName + '/api/auth/user', config)
            .then(async (response) => {                
                registerData["email"] = response.data.username
                registerData["firstname"] = response.data.name.split(" ")[0]
                registerData["lastname"] = response.data.name.split(" ")[1]

                self.setState({
                    waiting: false,
                    registerData: registerData
                })                                
                
            })
            .catch((error) => {
                Toast.show('Error happend. Please try again.');                
                self.setState({
                    waiting: false
                })
            });

    }

    saveUser = () => {
        const { registerDataError, registerData } = this.state
        var isError = false
        console.warn(registerData);
        {
            fields.map((field, index) => {
                let fieldError = validate(field.validationType, registerData[field.title])
                registerDataError[field.title] = fieldError;
            })
        }

        this.setState({
            registerDataError: registerDataError
        })

        fields.map((field, index) => {
            if (registerDataError[field.title]) {
                isError = true
                return
            }
        })

        if (isError) {
            Toast.show('Check your input fields.');
        }
        else {
            axios.post('save', registerData).then((resp) => {
                console.log(resp)
            }).catch(err => {
                console.log(err)
            })
        }
    };

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: { uri: res.uri }
                });

            }
        });
    }

    getInputElement(field, index) {
        const { registerDataError, registerData } = this.state
        return (
            <View key={index}>
                <FormLabel labelStyle={styles.formLabels}>{field.title} <Text style={styles.formRequiredInputs}>{field.required ? "*" : null}</Text></FormLabel>
                <FormInput  value={registerData[field.key]} containerStyle={styles.formInputContainer} keyboardType={field.validationType=="email" ? 'email-address' : field.validationType=="number" ? 'numeric' : 'default'} onChangeText={value => { registerData[field.key] = value, this.setState({ registerData: registerData }) }} inputStyle={styles.formInputs} secureTextEntry={field.security == null ? false : field.security} />
                {registerDataError[field.key] ? <FormValidationMessage labelStyle={styles.formValidationMessage}>{registerDataError[field.key]}</FormValidationMessage> : null}
            </View>
        )
    }

    getDatePickerElement(field, index) {
        const { registerDataError, registerData } = this.state
        return (
            <View key={index}>
                <FormLabel labelStyle={styles.formLabels}>{field.title} <Text style={styles.formRequiredInputs}>{field.required ? "*" : null}</Text></FormLabel>
                <DatePicker
                    style={styles.datepicker}
                    date={registerData[field.key]}
                    mode="date"
                    placeholder={"Please Select"}
                    format="YYYY-MM-DD"
                    minDate="1950-01-01"
                    maxDate="2050-12-31"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0,
                        },
                        dateInput: {
                            marginLeft: 35,
                            borderWidth: 0,
                            borderColor: '#aaa',
                            justifyContent: 'center',
                            alignItems: 'flex-start',
                        },
                        dateTouchBody: {
                            // borderBottomWidth: 1,                                    
                            // borderBottomColor: 'rgba(0,0,0,0.3)',                                                   
                        },
                        dateText: {
                            color: '#fff',
                            fontSize: 16
                        },
                        placeholderText: {
                            color: '#fff',
                            fontSize: 16
                        },
                    }}
                    onDateChange={(date) => { registerData[field.key] = date, this.setState({ registerData: registerData }) }}
                />
                {registerDataError[field.key] ? <FormValidationMessage labelStyle={styles.formValidationMessage}>{registerDataError[field.key]}</FormValidationMessage> : null}
            </View>
        )
    }

    getPickerElement(field, index) {
        const { registerDataError, registerData} = this.state
        return (
            <View key={index}>
                <FormLabel labelStyle={styles.formLabels}>{field.title} <Text style={styles.formRequiredInputs}>{field.required ? "*" : null}</Text></FormLabel>
                {Platform.OS == 'android' ?
                    <Picker
                        selectedValue={registerData[field.key]}
                        style={styles.pickers | { width: '100%' }}
                        itemStyle={styles.pickerItem}
                        onValueChange={(itemValue, itemIndex) => { itemValue == "Please Select" ? registerData[field.key] = "" : registerData[field.key] = itemValue, this.setState({ registerData: registerData }) }}>
                        {
                            field.value.map((item, index) => (
                                <Picker.Item key={index} label={item.label} value={item.value} />
                            ))
                        }
                    </Picker> :
                    <IOSPicker
                        selectedValue={registerData[field.key]}
                        style={styles.pickers}
                        textStyle={styles.pickerItem}
                        mode={'modal'}
                        onValueChange={(itemValue, itemIndex) => { itemValue == "Please Select" ? registerData[field.key] = "" : registerData[field.key] = itemValue, this.setState({ registerData: registerData }) }}>
                        {
                            field.value.map((item, index) => (
                                <Picker.Item key={index} label={item.label} value={item.value} />
                            ))
                        }
                    </IOSPicker>
                }
                {registerDataError[field.key] ? <FormValidationMessage labelStyle={styles.formValidationMessage}>{registerDataError[field.key]}</FormValidationMessage> : null}
            </View>
        )
    }
    render() {
        const {waiting} = this.state

        return (
            <MainScreen waiting={waiting}>
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <TouchableOpacity style={styles.avatarContainer} onPress={this.pickImageHandler.bind(this)}>
                            {this.state.pickedImage == "" ?
                                <Icon name="camera" style={styles.avatarIcon} /> :
                                <Image source={this.state.pickedImage} style={styles.avatarImage} />}
                        </TouchableOpacity>

                        {
                            fields.map((field, index) => {
                                if (field.type == "input") {
                                    return this.getInputElement(field, index)
                                }
                                else if (field.type == "datepicker") {
                                    return this.getDatePickerElement(field, index)
                                }
                                else if (field.type == "picker") {
                                    return this.getPickerElement(field, index)
                                }
                            })
                        }

                        <TouchableOpacity style={styles.controlButtonContainer} onPress={this.saveUser}>
                            <Text style={styles.controlButtonText}>Save</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </KeyboardAwareScrollView>
            </MainScreen>
        )
    }
}

export default ProfileScreen;
