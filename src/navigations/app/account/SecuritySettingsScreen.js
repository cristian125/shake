import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {
  FormValidationMessage,
  FormInput,
  Input,
  CheckBox,
  Icon
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors, fonts } from "../../../styles/styles";
import { MainScreen } from "../../../components";
import { validate } from "../../../utils/validation";
import Constants from "../../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import SelectionModal from "../../../components/SelectionModal";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FeatherIcon from "react-native-vector-icons/Feather";
import EntypoIcon from "react-native-vector-icons/Entypo";

class SecuritySettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {      
      question: "",
      answer: "",
      
      questionError: null,
      answerError: null,

      waiting: false,
      isDisplayModal: false,
      userInfo: null
    };

    this.onNext = this.onNext.bind(this);
    this.onItemSelected = this.onItemSelected.bind(this);
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  onNext = () => {
    this.props.navigation.navigate("UpdatePassword");
  };

  onCloseSelectionModal = () => {
    this.setState({ isDisplayModal: false });
  };

  onItemSelected(value) {
    this.setState({ question: value });
  }

  render() {
    const {
      waiting,
      questionError,
      answerError,
      isDisplayModal
    } = this.state;

    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Security Settings</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            <Text style={styles.content_title}>Security Question</Text>
            {/* Question */}
            <View style={styles.inputContainer}>
              {/* <FontAwesomeIcon
                name="venus-mars"
                size={14}
                color={colors.standardColor}
              /> */}
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() => this.setState({ isDisplayModal: true })}
              >
                <Text
                  style={{
                    color: this.state.question ? "black" : colors.standardColor
                  }}
                >
                  {this.state.question
                    ? this.state.question
                    : "Choose a Security Question"}
                </Text>
                <FeatherIcon
                  name="chevron-down"
                  size={16}
                  color={colors.redColor}
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {questionError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {questionError}
              </FormValidationMessage>
            ) : null}

            {/* Answer */}
            <View style={styles.inputContainer}>
              {/* <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              /> */}
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Your Answer"
                onChangeText={value =>
                  this.setState({ answer: value }, function() {})
                }
                value={this.state.answer}
              />
            </View>
            <Text style={styles.underline} />
            {answerError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {answerError}
              </FormValidationMessage>
            ) : null}
            
            <TouchableOpacity
              style={[styles.controlButtonContainer, { marginTop: 30 }]}
              onPress={this.onNext}
            >
              <LinearGradient
                style={styles.controlButtonLinearGradient}
                start={{ x: -0.2, y: 1.5 }}
                end={{ x: 1.2, y: -0.5 }}
                colors={[colors.linearStarColor, colors.linearEndColor]}
              >
                <Text style={styles.controlButtonText}>NEXT</Text>
              </LinearGradient>
            </TouchableOpacity>          
          </View>
        </KeyboardAwareScrollView>

        <SelectionModal
          isShown={isDisplayModal}
          closeModal
          closeModal={this.onCloseSelectionModal}
          list={[
            "What is your first pet name?",
            "What is your favorite band?",
            "What is your mother maiden name?",
            "What is the name of your favorite pet?"
          ]}
          value={this.state.question}
          itemClicked={value => this.onItemSelected(value)}
        />
      </MainScreen>
    );
  }
}

export default SecuritySettingsScreen;
