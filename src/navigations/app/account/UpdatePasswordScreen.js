import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {
  FormValidationMessage,
  FormInput,
  Input,
  CheckBox,
  Icon
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors, fonts } from "../../../styles/styles";
import { MainScreen } from "../../../components";
import { validate } from "../../../utils/validation";
import Constants from "../../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import SelectionModal from "../../../components/SelectionModal";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FeatherIcon from "react-native-vector-icons/Feather";
import EntypoIcon from "react-native-vector-icons/Entypo";

class UpdatePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      password: "",
      passwordConfirmation: "",      

      oldPasswordError: null,
      passwordError: null,
      passwordConfirmationError: null,      

      waiting: false,
      isDisplayModal: false,
      userInfo: null
    };

    this.updatePassword = this.updatePassword.bind(this);    
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  updatePassword = () => {
    this.props.navigation.popToTop();
  };

  render() {
    const {
      waiting,
      oldPasswordError,
      passwordError,
      passwordConfirmationError,      
      isDisplayModal
    } = this.state;

    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>Update Password</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            {/* Old Password */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Old Password"
                secureTextEntry
                onChangeText={value => this.setState({ oldPassword: value })}
                value={this.state.oldPassword}
              />
            </View>
            <Text style={styles.underline} />
            {oldPasswordError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {oldPasswordError}
              </FormValidationMessage>
            ) : null}

            {/* New Password */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="New Password"
                secureTextEntry
                onChangeText={value => this.setState({ password: value })}
                value={this.state.password}
              />
            </View>
            <Text style={styles.underline} />
            {passwordError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {passwordError}
              </FormValidationMessage>
            ) : null}

            {/* Password Confirmation */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="lock-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Re-Type New Password"
                secureTextEntry
                onChangeText={value =>
                  this.setState({ passwordConfirmation: value })
                }
                value={this.state.passwordConfirmation}
              />
            </View>
            <Text style={styles.underline} />
            {passwordConfirmationError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {passwordConfirmationError}
              </FormValidationMessage>
            ) : null}

            <TouchableOpacity
              style={[styles.controlButtonContainer, { marginTop: 30 }]}
              onPress={this.updatePassword}
            >
              <LinearGradient
                style={styles.controlButtonLinearGradient}
                start={{ x: -0.2, y: 1.5 }}
                end={{ x: 1.2, y: -0.5 }}
                colors={[colors.linearStarColor, colors.linearEndColor]}
              >
                <Text style={styles.controlButtonText}>UPDATE</Text>
              </LinearGradient>
            </TouchableOpacity>          
          </View>
        </KeyboardAwareScrollView>
      </MainScreen>
    );
  }
}

export default UpdatePasswordScreen;
