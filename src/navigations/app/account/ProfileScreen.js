import React, { Component } from "react";
import axios from "axios";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  AsyncStorage,
  ActivityIndicator,
  AppState
} from "react-native";
import {
  FormValidationMessage,
  FormInput,
  Input,
  CheckBox,
  Icon
} from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import styles, { colors, fonts } from "../../../styles/styles";
import { MainScreen } from "../../../components";
import { validate } from "../../../utils/validation";
import Toast from 'react-native-easy-toast'
import Constants from "../../../utils/constants";
import LinearGradient from "react-native-linear-gradient";
import SelectionModal from "../../../components/SelectionModal";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FeatherIcon from "react-native-vector-icons/Feather";
import EntypoIcon from "react-native-vector-icons/Entypo";
import DatePicker from "react-native-date-picker";
import moment from "moment";
import ImagePicker from "react-native-image-picker";
var RNFS = require("react-native-fs");

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickedImage: "",
      firstName: "",
      lastName: "",
      gender: "",
      birthDate: null,
      country: "",

      firstNameError: null,
      lastNameError: null,
      genderError: null,
      birthDateError: null,
      countryError: null,

      waiting: false,
      isDisplayModal: false,
      userInfo: null,
      isCountrySelected: false,
      countryList: [],
      avatar: "",
      isCalenderShown: false
    };

    this.registerUser = this.registerUser.bind(this);
    this.onItemSelected = this.onItemSelected.bind(this);
    this.getUser = this.getUser.bind(this);
  }
  static navigationOptions = {
    title: "",
    header: null
  };

  registerUser = async () => {
    const self = this;

    let {
      firstName,
      lastName,
      gender,
      birthDate,
      country,
      userInfo,
      pickedImage
    } = this.state;

    let firstNameError = validate("input", firstName);
    let lastNameError = validate("input", lastName);
    let genderError = validate("input", gender);
    let birthDateError = validate("input", this.getBirthDateString());
    let countryError = validate("input", country);

    this.setState({
      firstNameError: firstNameError,
      lastNameError: lastNameError,
      genderError: genderError,
      birthDateError: birthDateError,
      countryError: countryError
    });

    if (
      firstNameError ||
      lastNameError ||
      genderError ||
      birthDateError ||
      countryError
    ) {      
      this.refs.toast.show("Check your input fields.");
      return;
    }

    // this.props.navigation.navigate('App');
    var userToken = "";
    await AsyncStorage.getItem("userToken").then(value => {
      userToken = value;
    });

    let config = {
      headers: {
        Authorization: "Bearer " + userToken,
        "Content-Type": "application/json"
      }
    };

    let payload = {
      id: userInfo.id,
      first_name: firstName,
      last_name: lastName,
      gender: gender,
      dob: this.getBirthDateString(),
      country: country,
      avatar: pickedImage
    };

    this.setState({
      waiting: true
    });
    axios
      .post(Constants.webserviceName + "/api/auth/update", payload, config)
      .then(response => {
        self.setState({
          waiting: false
        });
        this.props.navigation.state.params.getUser();        
        self.refs.toast.show("Updated Successfully.");
      })
      .catch(error => {        
        let message = JSON.parse(error.request.response)                
       
        let firstNameError = message.errors.detail.first_name ? message.errors.detail.first_name : null;
        let lastNameError = message.errors.detail.last_name ? message.errors.detail.last_name : null;
        let genderError = message.errors.detail.gender ? message.errors.detail.gender : null;
        let birthDateError = message.errors.detail.dob ? message.errors.detail.dob : null;
        let countryError = message.errors.detail.country ? message.errors.detail.country : null;
      
        self.setState({
          waiting: false,
          firstNameError: firstNameError,
          lastNameError: lastNameError,
          genderError: genderError,
          birthDateError: birthDateError,
          countryError: countryError
        });
        
        self.refs.toast.show("Error happend. Please check your inputs and try again.");
      });
  };

  onCloseSelectionModal = () => {
    this.setState({ isDisplayModal: false });
  };

  onItemSelected(value) {
    if (this.state.isCountrySelected) this.setState({ country: value });
    else this.setState({ gender: value });
  }

  pickImageHandler = () => {
    ImagePicker.showImagePicker(
      { title: "Pick an Image", maxWidth: 800, maxHeight: 600 },
      async res => {
        if (res.didCancel) {
          console.log("User cancelled!");
        } else if (res.error) {
          console.log("Error", res.error);
        } else {
          var avatar_b64data = await RNFS.readFile(res.uri, "base64").then();
          this.setState({
            pickedImage: avatar_b64data
          });
        }
      }
    );
  };

  componentWillMount() {
    this.getUser();
  }

  getUser = async () => {
    const self = this;
    var userToken = "";
    await AsyncStorage.getItem("userToken").then(value => {
      userToken = value;
    });

    let config = {
      headers: {
        Authorization: "Bearer " + userToken,
        "Content-Type": "application/json"
      }
    };

    this.setState({
      waiting: true
    });

    var countryList = [];
    axios
      .get(Constants.webserviceName + "/api/auth/country", config)
      .then(async response => {
        for (var index = 0; index < response.data.data.length; index++) {
          countryList.push(response.data.data[index].name);
        }
      })
      .catch(error => {});

    axios
      .get(Constants.webserviceName + "/api/auth/user", config)
      .then(async response => {        
        self.setState({
          waiting: false,
          userInfo: response.data,
          firstName: response.data.first_name,
          lastName: response.data.last_name,
          gender: response.data.gender,
          birthDate: new Date(response.data.dob),
          country: response.data.country,
          // pickedImage: response.data.profile_image,
          countryList: countryList
        });
      })
      .catch(error => {
        self.setState({
          waiting: false
        });
      });
  };

  getBirthDateString() {
    if (!this.state.birthDate) return "";

    var dateString = moment(this.state.birthDate).format("YYYY-MM-DD");
    return dateString;
  }

  render() {
    const {
      waiting,
      firstNameError,
      lastNameError,
      genderError,
      birthDateError,
      countryError,
      isDisplayModal,
      isCountrySelected,
      userInfo,
      countryList,
      isCalenderShown
    } = this.state;

    return (
      <MainScreen waiting={waiting}>
        <View style={styles.topbar}>
          <LinearGradient
            style={styles.topBarLinearLayout}
            start={{ x: -0.2, y: 1.5 }}
            end={{ x: 1.2, y: -0.5 }}
            colors={[colors.linearStarColor, colors.linearEndColor]}
          >
            <Text style={styles.topbarCenterTitle}>My Account</Text>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
              style={styles.backButton}
            >
              <MaterialIcons
                name="keyboard-backspace"
                size={24}
                color="white"
              />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.scroll_container}>
          <View style={[styles.scroll_content_wrapper, {paddingTop: 100}]}>
            {/* Profile Info*/}
            <View style={styles.profile_info_container}>
              <TouchableOpacity
                style={styles.avatarContainer}
                onPress={this.pickImageHandler.bind(this)}
              >
                {this.state.pickedImage == "" ? (
                  userInfo && userInfo.profile_image ? (
                    <Image
                      source={{
                        uri:
                          Constants.webserviceName +
                          "/image/" +
                          userInfo.profile_image
                      }}
                      style={styles.avatarImage}
                    />
                  ) : (
                    <EntypoIcon
                      name="user"
                      size={90}
                      color={"white"}
                      style={styles.avatarIcon}
                    />
                  )
                ) : (
                  <Image
                    source={{
                      uri: `data:image/png;base64,${this.state.pickedImage}`
                    }}
                    style={styles.avatarImage}
                  />
                )}
              </TouchableOpacity>
              <View style={styles.profile_info_detail_view}>
                <Text style={styles.profile_info_user_email}>
                  {userInfo && userInfo.email ? userInfo.email : null}
                </Text>
                <Text style={styles.profile_info_username}>
                  {userInfo && userInfo.username ? userInfo.username : null}
                </Text>
              </View>
            </View>

            {/* First Name */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="First Name"
                onChangeText={value =>
                  this.setState({ firstName: value }, function() {})
                }
                value={this.state.firstName}
              />
            </View>
            <Text style={styles.underline} />
            {firstNameError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {firstNameError}
              </FormValidationMessage>
            ) : null}

            {/* Last Name */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="account-outline"
                size={16}
                color={colors.standardColor}
              />
              <TextInput
                style={styles.inputs}
                placeholderTextColor={colors.standardColor}
                placeholder="Last Name"
                onChangeText={value =>
                  this.setState({ lastName: value }, function() {})
                }
                value={this.state.lastName}
              />
            </View>
            <Text style={styles.underline} />
            {lastNameError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {lastNameError}
              </FormValidationMessage>
            ) : null}

            {/* Gender */}
            <View style={styles.inputContainer}>
              <FontAwesomeIcon
                name="venus-mars"
                size={14}
                color={colors.standardColor}
              />
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() =>
                  this.setState({
                    isDisplayModal: true,
                    isCountrySelected: false
                  })
                }
              >
                <Text
                  style={{
                    color: this.state.gender ? "black" : colors.standardColor
                  }}
                >
                  {this.state.gender ? this.state.gender : "Gender"}
                </Text>
                <FeatherIcon
                  name="chevron-down"
                  size={16}
                  color={colors.redColor}
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {genderError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {genderError}
              </FormValidationMessage>
            ) : null}

            {/* BirthDate */}
            <View style={styles.inputContainer}>
              <FeatherIcon
                name="calendar"
                size={14}
                color={colors.standardColor}
              />
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() =>
                  this.setState({
                    isCalenderShown: !this.state.isCalenderShown
                  })
                }
              >
                <Text
                  style={{
                    color: this.getBirthDateString() ? "black" : colors.standardColor
                  }}
                >
                  {this.getBirthDateString()
                    ? this.getBirthDateString()
                    : "Date of Birth"}
                </Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {birthDateError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {birthDateError}
              </FormValidationMessage>
            ) : null}

            {isCalenderShown && (
              <DatePicker
                date={this.state.birthDate ? this.state.birthDate : new Date(moment(new Date()).format("YYYY-MM-DD"))}
                style={styles.datepicker}
                mode={"date"}
                onDateChange={date => this.setState({ birthDate: date })}
              />
            )}

            {/* Country */}
            <View style={styles.inputContainer}>
              <MaterialCommunityIcon
                name="home-outline"
                size={16}
                color={colors.standardColor}
              />
              <TouchableOpacity
                style={styles.inputSelection}
                onPress={() =>
                  this.setState({
                    isDisplayModal: true,
                    isCountrySelected: true
                  })
                }
              >
                <Text
                  style={{
                    color: this.state.country ? "black" : colors.standardColor
                  }}
                >
                  {this.state.country ? this.state.country : "Country"}
                </Text>
                <FeatherIcon
                  name="chevron-down"
                  size={16}
                  color={colors.redColor}
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.underline} />
            {countryError ? (
              <FormValidationMessage labelStyle={styles.formValidationMessage}>
                {countryError}
              </FormValidationMessage>
            ) : null}

            <TouchableOpacity
              style={[styles.controlButtonContainer, { marginTop: 30 }]}
              onPress={this.registerUser}
            >
              <LinearGradient
                style={styles.controlButtonLinearGradient}
                start={{ x: -0.2, y: 1.5 }}
                end={{ x: 1.2, y: -0.5 }}
                colors={[colors.linearStarColor, colors.linearEndColor]}
              >
                <Text style={styles.controlButtonText}>SAVE MY PROFILE</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>        

        <SelectionModal
          isShown={isDisplayModal}
          closeModal
          closeModal={this.onCloseSelectionModal}
          list={isCountrySelected ? countryList : ["Female", "Male", "Other"]}
          value={isCountrySelected ? this.state.country : this.state.gender}
          itemClicked={value => this.onItemSelected(value)}
        />
        <Toast ref="toast"/>  
      </MainScreen>
    );
  }
}

export default ProfileScreen;
