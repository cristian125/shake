import { StyleSheet, Dimensions, Platform } from "react-native";
import { red, black } from "ansi-colors";
const { width, height } = Dimensions.get("window");

export const colors = {
  linearStarColor: "rgb(222, 77, 71)",
  linearEndColor: "rgb(240, 170, 24)",
  standardColor: "rgb(169, 169, 176)",
  redColor: "rgb(221, 80, 63)",
  onlineColor: "#38d86b",
  purpleColor: "#743d74",
  purpleLightColor: "#b87ebb",
  yellowColor: "#f5a623"
};

export const fonts = {
  defualtSize: 15,
  titleSize: 20,
  mediumSize: 18,
  smallSize: 12,
  minSize: 10
};

export default (styles = StyleSheet.create({
  pageBackground: {
    backgroundColor: "white",
    width: "100%",
    height: "100%"
    // justifyContent: "center",
    // padding: 15
  },

  mainContainer: {
    width: "100%",
    height: "100%",
    justifyContent: "center",    
  },

  scroll_container: {
    flexGrow: 1,
    justifyContent: "center",    
  },
  scroll_content_wrapper: {
    width: "100%",    
    justifyContent: "center",
    padding: 20,    
  },

  logo_wrapper: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  logo_image: {
    resizeMode: "contain",
    alignSelf: "center"
  },
  logoTitle: {
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
    color: "black"
  },

  contentContainer: {
    height: "100%",
    width: "100%",
    marginBottom: 30,
    marginTop: 30,
    flex: 1
  },

  inputContainer: {
    height: 45,
    marginTop: 20,
    fontSize: fonts.defualtSize,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
    flexDirection: "row",
    width: "100%"
  },
  inputs: {
    height: "100%",
    flex: 1,
    fontSize: fonts.defualtSize,
    paddingLeft: 10
  },
  inputSelection: {
    height: "100%",
    flex: 1,
    fontSize: fonts.defualtSize,
    paddingLeft: 10,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  datepicker: {
    width: '100%', backgroundColor:'white', marginTop: 20, marginBottom: 0, paddingBottom: 0
  },
  underline: {
    height: 1,
    backgroundColor: colors.standardColor
  },
  formInputs: {
    fontSize: fonts.defualtSize,
    color: "#fff",
    paddingLeft: 0
  },
  formRequiredInputs: {
    color: "rgba(255,0,0,0.5)"
  },
  formInputContainer: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(255,255,255,0.5)",
    marginLeft: 0,
    marginRight: 0
  },
  formLabels: {
    fontSize: 15,
    marginLeft: 0,
    color: "rgba(255,255,255,0.5)"
  },
  formValidationMessage: {
    marginLeft: 0
  },
  pickers: {
    height: 50,
    color: "white",
    margin: 0,
    padding: 0,
    justifyContent: "center",
    alignItems: "flex-start",
    marginVertical: 0,
    marginHorizontal: 0,
    paddingVertical: 0,
    paddingHorizontal: 0,
    backgroundColor: "transparent",
    borderTopWidth: 0
  },
  pickerItem: {
    color: "white",
    textAlign: "center",
    fontSize: fonts.defualtSize
  },
  controlButtonContainer: {
    height: 50,
    marginTop: 10
  },
  controlButtonLinearGradient: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 5
  },
  controlButtonText: {
    color: "#fff",
    fontSize: fonts.defualtSize
  },

  signupForgotPasswordContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    height: 50
  },
  signupDescription: {
    color: colors.standardColor,
    fontSize: fonts.defualtSize
  },
  signupButtonContainer: {
    flex: 1
  },
  signUpButtonText: {
    color: colors.redColor,
    fontSize: fonts.defualtSize
  },
  forgotButtonContainer: {
    flex: 2
  },
  forgotButtonText: {
    color: colors.redColor,
    fontSize: fonts.defualtSize,
    textAlign: "right"
  },
  forgotPasswordDescription: {
    color: colors.standardColor,
    fontSize: fonts.defualtSize,
    textAlign: "center"
  },

  topbar: {
    width: "100%",
    height: 100,
    position: "absolute",
    top: 0,
    zIndex: 1
  },
  topBarLinearLayout: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  topbarCenterTitle: {
    color: "white",
    position: "absolute",
    width: "100%",
    textAlign: "center",
    fontSize: fonts.titleSize
  },
  backButton: {
    marginLeft: 10,
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  topbarLeftTitle: {
    marginLeft: 20,
    color: "white",
    textAlign: "left",
    fontSize: fonts.titleSize
  },
  menuButton: {
    marginRight: 10,
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  menuIcon: {
    resizeMode: "contain",
    width: 20,
    height: 14
  },

  menu_container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "red"
  },
  menu_linear_bg: {
    width: "100%",
    height: "100%",
    padding: 20
  },
  menu_close: {
    width: 40,
    height: 40,
    marginRight: -10,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end"
  },

  terms_conditions_title: {
    fontSize: fonts.defualtSize,
    color: colors.standardColor,
    textAlign: "center",
    marginTop: 30
  },
  btn_terms_conditions: {
    fontSize: fonts.defualtSize,
    color: colors.redColor,
    textAlign: "center"
  },

  checkbox_text: {
    color: colors.standardColor,
    fontWeight: "normal"
  },
  checkbox_container: {
    backgroundColor: "transparent",
    borderColor: "transparent",
    height: "100%",
    flex: 1,
    padding: 0,
    marginLeft: 0,
    justifyContent: "center"
  },

  profile_container: {
    position: "absolute",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    top: 40
  },
  profile_avatar_container: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: "#e0e0e0"
  },
  profile_avatar: {
    width: "100%",
    height: "100%",
    borderRadius: 50,
    resizeMode: "cover"
  },
  profile_online_status: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: colors.onlineColor,
    borderColor: "white",
    borderWidth: 2,
    position: "absolute",
    bottom: 5,
    right: 5
  },
  user_fullname: {
    color: "white",
    fontSize: fonts.mediumSize,
    marginTop: 5
  },
  username: {
    color: "white",
    fontSize: fonts.smallSize,
    marginTop: 5
  },
  profile_status_view: {
    backgroundColor: "#fff",
    height: 80,
    width: "90%",
    position: "absolute",
    bottom: -40,
    borderRadius: 10,
    alignSelf: "center",
    zIndex: 1,
    flexDirection: "row",
    alignItems: "center",

    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 5
  },
  profile_status_item: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  profile_status_value: {
    fontSize: fonts.defualtSize,
    color: "#743d74"
  },
  profile_status_title: {
    fontSize: fonts.minSize,
    color: "#b87ebb"
  },

  item_underline: {
    color: "black",
    width: "100%",
    height: 1,
    backgroundColor: "#f4f4f4"
  },
  account_item: {
    display: "flex",
    flexDirection: "column",
    height: 80
  },
  account_item_content: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  account_item_text_container: {
    flex: 1,
    marginLeft: 10
  },
  account_item_title: {
    fontSize: fonts.defualtSize,
    color: colors.redColor
  },
  account_item_description: {
    fontSize: fonts.smallSize,
    color: colors.standardColor
  },
  account_item_icon: {
    width: 30,
    textAlign: "center"
  },

  home_search_container: {
    backgroundColor: "#fff",
    height: 45,
    width: "85%",
    position: "absolute",
    bottom: -22,
    borderRadius: 23,
    alignSelf: "center",
    zIndex: 1,
    flexDirection: "row",
    alignItems: "center",

    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 5
  },
  home_search_icon: {
    width: 40,
    textAlign: "center"
  },

  home_list_item: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    marginBottom: 10
  },
  home_list_item_heading_container: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40
  },
  home_list_item_title: {
    color: colors.purpleColor,
    fontSize: fonts.defualtSize,
    height: "100%",
    textAlignVertical: "center",
    paddingLeft: 10
  },
  btn_seeall: {
    color: colors.redColor,
    fontSize: fonts.smallSize,
    height: "100%",
    textAlignVertical: "center",
    width: 60
  },

  reader_item: {
    width: 130,
    alignItems: "center"
  },
  reader_info_container: {
    width: 110,
    marginTop: 10,
    alignItems: "flex-start"
  },
  reader_fullname: {
    color: colors.purpleColor,
    fontSize: fonts.defualtSize,
    textAlign: "left"
  },

  myStarStyle: {
    color: colors.yellowColor
  },
  myEmptyStarStyle: {
    color: colors.standardColor
  },

  category_item: {
    width: 140,
    height: 120,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10
  },
  category_item_image: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "#505050",
    overflow: "hidden",
    resizeMode: "cover"
  },
  category_item_title: {
    padding: 5,
    fontSize: fonts.smallSize,
    color: "white",
    textAlign: "center"
  },

  post_item: {
    width: 190,
    height: 170,
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10
  },
  post_item_image: {
    width: "100%",
    height: 100,
    borderRadius: 10,
    backgroundColor: "#505050"
  },
  post_item_info: {
    width: "100%"
  },
  post_item_title: {
    marginTop: 5,
    fontSize: fonts.smallSize,
    color: colors.purpleColor
  },
  post_item_user_info: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  post_item_user_avatar: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#e0e0e0",
    resizeMode: "cover",
    overflow: "hidden"
  },
  post_item_username: {
    color: colors.purpleColor,
    fontSize: fonts.minSize,
    paddingLeft: 5,
    paddingRight: 5
  },

  featured_reader_item: {
    height: 120,
    width: 300,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
    display: "flex",
    flexDirection: "row",
    overflow: "hidden"
  },
  featured_reader_avatar_container: {
    height: "100%",
    width: 100,
    backgroundColor: "#505050",
    flexDirection: "row",
    alignItems: "flex-end"
  },
  featured_reader_avatar_controls: {
    flex: 1,
    padding: 10,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  featured_reader_info_container: {
    flex: 1,
    height: "100%",
    backgroundColor: "white",
    display: "flex",
    flexDirection: "column",
    padding: 10,
    alignItems: "flex-start"
  },
  featured_reader_title_container: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  featured_reader_fullname: {
    fontSize: fonts.defualtSize,
    color: "black"
  },
  featured_reader_online_status: {
    backgroundColor: colors.onlineColor,
    width: 14,
    height: 14,
    borderRadius: 7,
    marginLeft: 10
  },
  featured_reader_job: {
    fontSize: fonts.minSize,
    color: colors.purpleLightColor,
    marginBottom: 5
  },
  featured_Reader_info_text: {
    marginTop: 5,
    fontSize: fonts.smallSize,
    color: colors.standardColor
  },

  promotion_item: {
    width: width,
    height: width / 2,
    paddingLeft: 10,
    paddingRight: 10
  },
  pormotion_item_image: {
    width: "100%",
    height: "100%",
    overflow: "hidden",
    borderRadius: 10,
    backgroundColor: "#505050"
  },

  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around",
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  modal_list_bg_wrapper: {
    backgroundColor: "#fff",
    flexDirection: "column",
    width: "80%",
    maxHeight: "70%",
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    elevation: 5
  },
  modal_list_bg: {
    width: "100%",
    paddingTop: 10
  },
  selection_item: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 5,
    marginBottom: 5,
    display: "flex",
    flexDirection: "column",
    height: 40
  },
  selection_item_content: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  selection_item_title: {
    color: "black",
    flex: 1,
    fontSize: fonts.defualtSize
  },

  profile_info_container: {
    marginTop: 30,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  avatarContainer: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: "#dedede",    
    justifyContent: "center",
    alignItems: "center", 
    overflow:'hidden',
  },
  avatarImage: {
    height: "100%",
    width: "100%",
    borderRadius: 50,
    overflow:'hidden',
    resizeMode:'cover'
  },
  avatarIcon: {
    marginBottom: -30,
    overflow:'hidden',
  },
  profile_info_detail_view: {
    marginLeft: 20,    
    flexDirection:'column',
    alignItems:'flex-start',    
  },
  profile_info_user_email: {
    fontSize: fonts.defualtSize,
    color: colors.purpleColor,    
  },
  profile_info_username: {
    fontSize: fonts.smallSize,
    color: colors.standardColor,
    marginTop: 10,
  },

  content_title: {
    fontSize: fonts.mediumSize,
    color: 'black',
    marginTop: 30,
  }
}));
