import React from "react";
import {
  AuthLoadingScreen,
  LoginScreen,
  ForgotPasswordScreen,
  SignupScreen,
  VerificationCodeScreen,
  ResetPasswordScreen,
  HomeScreen,
  OurReadersScreen,
  ArticlesScreen,
  AccountScreen,
  ProfileScreen,
  SecuritySettingsScreen,
  UpdatePasswordScreen
} from "../navigations";
import {
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator,
  createBottomTabNavigator,
  StackNavigator,
  DrawerNavigator,
  SwitchNavigator,
  DrawerItems,
  DrawerView
} from "react-navigation";
import { TouchableOpacity, AsyncStorage, Dimensions } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { colors } from "../styles/styles";
import MenuScreen from "../navigations/app/MenuScreen";

const { width } = Dimensions.get("screen");

const AuthRouter = StackNavigator(
  {
    Login: LoginScreen,
    ForgotPassword: ForgotPasswordScreen,
    SignUp: SignupScreen,
    VerificationCode: VerificationCodeScreen,
    ResetPassword: ResetPasswordScreen
  },
  {
    initialRouteName: "Login"
  }
);

const AppTapRouter = createBottomTabNavigator(
  {
    Home: HomeScreen,
    OurReaders: OurReadersScreen,
    Articles: ArticlesScreen,
    Account: AccountScreen
  },
  {
    initialRouteName: "Home",
    tabBarOptions: {
      activeTintColor: colors.redColor,
      inactiveTintColor: colors.standardColor
    }
  }
);

const AppStackRouter = StackNavigator(
  {
    AppContent: AppTapRouter,

    //Account
    Profile: ProfileScreen,
    SecuritySettings: SecuritySettingsScreen,
    UpdatePassword: UpdatePasswordScreen
  },
  {
    initialRouteName: "AppContent",
    navigationOptions : {
      header: null
    }
  }
);


const DrawerRouter = createDrawerNavigator(
  {
    Home: AppStackRouter
  },
  {
    initialRouteName: "Home",
    contentComponent: MenuScreen,
    drawerWidth: width
  }
);

const ReactRouter = SwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthRouter,
    App: DrawerRouter
  },
  {
    initialRouteName: "AuthLoading",
    animationEnabled: false
  }
);
export default ReactRouter;
