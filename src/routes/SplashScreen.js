import React, { Component } from "react";
import { View, Text, StatusBar, Image } from "react-native";
import styles from "../styles/styles";
import { MainScreen } from "../components";
const Logo = require("../assets/images/ic_logo.png");

class SplashScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <MainScreen>
        <View style={styles.logo_wrapper}>
          <Image style={styles.logo_image} source={Logo} />
          <Text style={styles.logoTitle}>Psychic Contact</Text>
        </View>
      </MainScreen>
    );
  }
}

export default SplashScreen;
