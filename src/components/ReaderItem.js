import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";
import Stars from "react-native-stars";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const profileAvatar = require("../assets/images/img_profile_avatar.png");

const ReaderItem = props => {
  return (
    <TouchableOpacity style={styles.reader_item}>
      <View style={styles.profile_avatar_container}>
        <Image source={profileAvatar} style={styles.profile_avatar} />
        <View style={styles.profile_online_status} />
      </View>
      <View style={styles.reader_info_container}>
        <Text style={styles.reader_fullname}>{props.name}</Text>
        <Stars          
          default={props.rating}
          count={5}
          half={true}
          starSize={10}
          fullStar={<Icon name={"star"} style={[styles.myStarStyle]} />}
          emptyStar={
            <Icon
              name={"star-outline"}
              style={[styles.myStarStyle, styles.myEmptyStarStyle]}
            />
          }
          halfStar={<Icon name={"star-half"} style={[styles.myStarStyle]} />}
        />
      </View>
    </TouchableOpacity>
  );
};

export default ReaderItem;
