import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";
import HomeListItemHeader from "./HomeListItemHeader"
import CategoryItem from "./CategoryItem";

const CategoryList = props => {
  return (
    <View style={styles.home_list_item}>
      <HomeListItemHeader title={props.title} />
      <ScrollView horizontal
      showsHorizontalScrollIndicator={false}
       style={{flex:1}}>
        <CategoryItem title={"LOVE & RELATIONSHIPS"}/>
        <CategoryItem title={"METAPHYSICAL PRACTICES"}/>
        <CategoryItem title={"LIFE COACHING"}/>       
        <CategoryItem title={"LOVE & RELATIONSHIPS"}/>
        <CategoryItem title={"METAPHYSICAL PRACTICES"}/>
        <CategoryItem title={"LIFE COACHING"}/>        
      </ScrollView>
    </View>
  );
};

export default CategoryList;
