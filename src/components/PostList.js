import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";

import HomeListItemHeader from "./HomeListItemHeader";
import PostItem from "./PostItem";

const PostList = props => {
  return (
    <View style={styles.home_list_item}>
      <HomeListItemHeader title={props.title} />
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <PostItem
          title="PATHWAYS TO HEADLING"
          username="Yemaya"
          rating={4}
        />
        <PostItem
          title="CONQUERING FEAR"
          username="Aleon"
          rating={4}
        />
        <PostItem
          title="PATHWAYS TO HEADLING"
          username="Yemaya"
          rating={4}
        />
        <PostItem
          title="CONQUERING FEAR"
          username="Aleon"
          rating={4}
        />
      </ScrollView>
    </View>
  );
};

export default PostList;
