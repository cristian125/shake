import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";

const HomeListItemHeader = props => {
  return (
    <View style={styles.home_list_item_heading_container}>
      <Text style={styles.home_list_item_title}>{props.title}</Text>
      {props.isShowSeeAll !== false ? (
        <TouchableOpacity>
          <Text style={styles.btn_seeall}>See All</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

export default HomeListItemHeader;
