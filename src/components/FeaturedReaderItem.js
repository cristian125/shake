import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import styles, { colors } from "../styles/styles";
import Stars from "react-native-stars";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";

const profileAvatar = require("../assets/images/img_profile_avatar.png");

const FeaturedReaderItem = props => {
  return (
    <View style={styles.featured_reader_item}>
      <ImageBackground source={profileAvatar} style={styles.featured_reader_avatar_container} imageStyle={{ resizeMode: "cover" }}>        
        <View style={styles.featured_reader_avatar_controls}>
        <TouchableOpacity><FontAwesomeIcon name={"envelope"} color={"white"} size={13}/></TouchableOpacity>
        <TouchableOpacity><Icon name={"phone"} color={"white"} size={14}/></TouchableOpacity>
        <TouchableOpacity><Icon name={"message-processing"} color={"white"} size={14}/></TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={styles.featured_reader_info_container}>      
        <View style={styles.featured_reader_title_container}>
          <Text style={styles.featured_reader_fullname}>{props.fullname}</Text>
          <View style={styles.featured_reader_online_status}/>
        </View>
        <Text style={styles.featured_reader_job}>{props.job}</Text>
        <Stars
          style={styles.featured_reader_star_view}
          default={props.rating}
          count={5}
          half={true}
          starSize={10}
          fullStar={<Icon name={"star"} style={[styles.myStarStyle]} />}
          emptyStar={
            <Icon
              name={"star-outline"}
              style={[styles.myStarStyle, styles.myEmptyStarStyle]}
            />
          }
          halfStar={<Icon name={"star-half"} style={[styles.myStarStyle]} />}
        />
        <Text style={styles.featured_Reader_info_text}>{props.text}</Text>
      </View>
    </View>
  );
};

export default FeaturedReaderItem;
