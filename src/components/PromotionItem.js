import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  ImageBackground
} from "react-native";
import styles, { colors } from "../styles/styles";

const promotionBG = require("../assets/images/img_promotion_bg.png");

const PromotionItem = props => {
  return (
    <TouchableOpacity style={styles.promotion_item}>
      <ImageBackground
        source={promotionBG}
        style={styles.pormotion_item_image}
        imageStyle={{ resizeMode: "cover" }}
      />
    </TouchableOpacity>
  );
};

export default PromotionItem;
