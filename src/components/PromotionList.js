import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";
import HomeListItemHeader from "./HomeListItemHeader";
import PromotionItem from "./PromotionItem";

const PromotionList = props => {
  return (
    <View style={styles.home_list_item}>
      <HomeListItemHeader title={props.title} />
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ flex: 1 }}
      >
      <PromotionItem />
      </ScrollView>
    </View>
  );
};

export default PromotionList;
