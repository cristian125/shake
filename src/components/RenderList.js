import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";
import ReaderItem from "./ReaderItem";
import HomeListItemHeader from "./HomeListItemHeader";

const RenderList = props => {
  return (
    <View style={styles.home_list_item}>
      <HomeListItemHeader title={props.title} />
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <ReaderItem name="Marlena Rain" rating={4} />
        <ReaderItem name="Maria Ayana" rating={4} />
        <ReaderItem name="Johnny Len" rating={4} />
        <ReaderItem name="Marlena Rain" rating={4} />
        <ReaderItem name="Maria Ayana" rating={4} />
        <ReaderItem name="Johnny Len" rating={4} />
      </ScrollView>
    </View>
  );
};

export default RenderList;
