import React, { Component } from "react";
import { Modal, TouchableOpacity, ScrollView } from "react-native";
import styles from "../styles/styles";
import SelectionItem from "./SelectionItem";

class SelectionModal extends Component { 
  constructor(props) {
    super(props);  

    this.itemClicked = this.itemClicked.bind(this);
  }

  itemClicked(value){    
    this.props.itemClicked(value);        
    this.props.closeModal();
  }
  render() {
    const { closeModal, list, value, isShown } = this.props;        

    return (
      <Modal
        transparent={true}
        animationType={"none"}
        visible={isShown}
        onRequestClose={() => {

        }}
      >
        <TouchableOpacity
          onPress={closeModal}
          style={styles.modalBackground}
          activeOpacity={1}
        >
          <TouchableOpacity
            activeOpacity={1}
            style={styles.modal_list_bg_wrapper}
          >
            <ScrollView
              style={styles.modal_list_bg}
              showsVerticalScrollIndicator={false}
            >        
            {list.map((item, index) => <SelectionItem key={index} itemClicked={() => this.itemClicked(item)} title={item} isChecked={item === value}/>)}      
            </ScrollView>
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    );
  }
}

export default SelectionModal;
