import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import styles, { colors } from "../styles/styles";

const categoryBG = require("../assets/images/img_category_bg.png");

const CategoryItem = props => {
  return (
    <TouchableOpacity style={styles.category_item}>
      <ImageBackground source={categoryBG} style={styles.category_item_image} imageStyle={{ resizeMode: "cover" }}>
        <Text style={styles.category_item_title}>{props.title}</Text>
      </ImageBackground>
    </TouchableOpacity>
  );
};

export default CategoryItem;
