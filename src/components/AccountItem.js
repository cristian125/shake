import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles, { colors } from "../styles/styles";
import FeatherIcon from "react-native-vector-icons/Feather";
import MaterialCommunityIcon from "react-native-vector-icons/MaterialCommunityIcons";
import IonIcon from "react-native-vector-icons/Ionicons";
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import SimpleLineIcon from "react-native-vector-icons/SimpleLineIcons";

const getIconType = iconType => {
  switch (iconType) {
    case "fund":
      return <SimpleLineIcon name="wallet" size={16} color={colors.redColor} style={styles.account_item_icon}/>;
    case "nrr":
      return (
        <MaterialCommunityIcon
          name="file-document-outline"
          size={20}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );
    case "email":
      return (
        <SimpleLineIcon
          name="envelope-letter"
          size={16}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );
    case "billing":
      return (
        <FeatherIcon name="credit-card" size={16} color={colors.redColor} style={styles.account_item_icon} />
      );
    case "history":
      return (
        <MaterialCommunityIcon
          name="history"
          size={20}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );
    case "message":
      return (
        <SimpleLineIcon name="envelope" size={16} color={colors.redColor} style={styles.account_item_icon} />
      );
    case "account":
      return (
        <MaterialCommunityIcon
          name="account-outline"
          size={20}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );
    case "security":
      return (
        <MaterialCommunityIcon
          name="lock-outline"
          size={18}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );    
    case "notification":
      return (
        <IonIcon
          name="ios-notifications-outline"
          size={20}
          color={colors.redColor} style={styles.account_item_icon}
        />
      );
    case "logout":
      return (
        <AntDesignIcon name="poweroff" size={14} color={colors.redColor} style={styles.account_item_icon} />
      );
  }
};

const AccountItem = props => {
  return (
    <View style={styles.account_item}>
      <TouchableOpacity
        style={styles.account_item_content}
        onPress={props.onPressItem}
      >
      {getIconType(props.iconType)}
        <View style={styles.account_item_text_container}>
          <Text style={styles.account_item_title}>{props.title}</Text>
          <Text style={styles.account_item_description}>
            {props.description}
          </Text>
        </View>
        <FeatherIcon name="chevron-right" size={16} color={colors.redColor}/>
      </TouchableOpacity>
      <View style={styles.item_underline} />
    </View>
  );
};

export default AccountItem;
