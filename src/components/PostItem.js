import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView, ImageBackground } from "react-native";
import styles, { colors } from "../styles/styles";
import Stars from "react-native-stars";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const postBG = require("../assets/images/img_post_bg.png");
const profileAvatar = require("../assets/images/img_profile_avatar.png");

const PostItem = props => {
  return (
    <TouchableOpacity style={styles.post_item}>
      <Image source={postBG} style={styles.post_item_image} />
      <View style={styles.post_item_info}>
        <Text style={styles.post_item_title}>{props.title}</Text>
        <View style={styles.post_item_user_info}>
          <Image source={profileAvatar} style={styles.post_item_user_avatar}/>
          <Text style={styles.post_item_username}>{props.username}</Text>
          <Stars
            style={styles.reader_star_view}
            default={props.rating}
            count={5}
            half={true}
            starSize={10}
            fullStar={<Icon name={"star"} style={[styles.myStarStyle]} />}
            emptyStar={
              <Icon
                name={"star-outline"}
                style={[styles.myStarStyle, styles.myEmptyStarStyle]}
              />
            }
            halfStar={<Icon name={"star-half"} style={[styles.myStarStyle]} />}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default PostItem;
