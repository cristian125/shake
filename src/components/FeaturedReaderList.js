import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";
import styles, { colors } from "../styles/styles";
import HomeListItemHeader from "./HomeListItemHeader";
import CategoryItem from "./CategoryItem";
import FeaturedReaderItem from "./FeaturedReaderItem";

const FeaturedReaderList = props => {
  return (
    <View style={styles.home_list_item}>
      <HomeListItemHeader title={props.title} isShowSeeAll={false} />
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{ flex: 1 }}
      >
        <FeaturedReaderItem
          fullname={"Trishia Lynn"}
          job={"Numerology, Astrology, Tarot..."}
          rating={4}
          text={"Lorem ipsum dolor sit amet, consectetur adipiscing elit..."}
        />
      </ScrollView>
    </View>
  );
};

export default FeaturedReaderList;
