import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import styles, { colors } from "../styles/styles";
import FeatherIcon from "react-native-vector-icons/Feather";

class SelectionItem extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { itemClicked, title, isChecked } = this.props;

    return (
      <View style={styles.selection_item} >
        <TouchableOpacity style={styles.selection_item_content} onPress={itemClicked}>
          <Text style={styles.selection_item_title}>{title}</Text>
          {isChecked ? <FeatherIcon name="check" size={16} color={colors.redColor} /> : null}
        </TouchableOpacity>
        <View style={[styles.underline, {backgroundColor: '#f0f0f0'}]} />
      </View>
    );
  }
}

export default SelectionItem;
